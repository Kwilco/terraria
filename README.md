# terraria

A 100% vanilla Terraria server, directly from https://terraria.org

Game version: `1.3.5.3`


## Docker Compose usage example

You can easily run this server with Docker Compose. In this example, I mount
`/opt/world` from the host in to `/world`. The arguments in `command` are passed
to the server invocation. You must specify `-world`, or the server will start in
interactive mode. `stdin_open` and `tty` need to be enabled, or the server will
crash trying to open `stdin`.

```yaml
services:
  terraria:
    image: "registry.gitlab.com/kwilco/terraria:latest"
    restart: always
    stdin_open: true
    tty: true
    ports:
      - "7777:7777"
    volumes:
      - /opt/terraria:/world
    command: ["-world", "/world/Tacoma.wld", "-players", "8", "-noupnp"]
```