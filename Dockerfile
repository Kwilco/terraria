FROM ubuntu:18.04 AS build

# Download tini
ADD https://github.com/krallin/tini/releases/download/v0.17.0/tini-static /sbin/tini
RUN chmod +x /sbin/tini

# Download Terraria server
RUN apt-get update && apt-get install -yq unzip

ADD https://terraria.org/server/terraria-server-1353.zip /tmp/server.zip
RUN unzip /tmp/server.zip -d /server
RUN chmod +x /server/1353/Linux/TerrariaServer.bin.x86_64


# Multi-stage build to keep size small
FROM ubuntu:18.04

COPY --from=build /sbin/tini /sbin/tini
COPY --from=build /server/1353/Linux/ /server

VOLUME /world
EXPOSE 7777

ENTRYPOINT ["/sbin/tini", "-g", "--", "/server/TerrariaServer.bin.x86_64"]
CMD [ "-players", "8", "-noupnp" ]
